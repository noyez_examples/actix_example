
extern crate actix_web;
use actix_web::{server, http, App, HttpRequest, HttpResponse, Path, Json, Query};
#[macro_use] extern crate serde_derive;
extern crate serde_urlencoded;


#[derive(Debug)]
struct StateOne {
    one: u32
}

#[derive(Debug)]
struct StateTwo {
    two: u32
}

#[derive(Debug, Deserialize)]
struct QueryParam {
    q: Option<String>
}

#[derive(Debug, Serialize, Deserialize)]
struct PostData {
    q: String
}

fn states(req: HttpRequest<(StateOne, StateTwo)>, info: Path<String> ) -> HttpResponse {
    let (state_one, state_two) = req.state();
    if let Ok(query_param) = serde_urlencoded::from_str::< QueryParam >(req.query_string())
    {
        println!("state_one: {:?}", state_one);
        println!("state_two: {:?}", state_two);
        println!("param: {:?}", info.to_string());
        println!("query param: {:?}", query_param);
    }
    HttpResponse::Ok().into()
}

fn states2(req: HttpRequest<(StateOne, StateTwo)>, info: Query<QueryParam> ) -> HttpResponse {
    let (state_one, state_two) = req.state();
    println!("state_one: {:?}", state_one);
    println!("state_two: {:?}", state_two);
    println!("query param: {:?}", info.q);
    HttpResponse::Ok().into()
}


fn post_states((post_data,  req): (Json<PostData>, HttpRequest<(StateOne, StateTwo)>), info: Path<String> ) -> HttpResponse {
    let (state_one, state_two) = req.state();
    println!("state_one: {:?}", state_one);
    println!("state_two: {:?}", state_two);
    println!("param: {:?}", info.to_string());
    println!("POST data: {:?}", post_data);
    HttpResponse::Ok().into()
}

fn main() {
    println!("curl \"http://localhost:8088/hello/states?q=query_param\"");
    println!("curl -H \"Content-Type: application/json\" -X POST -d '{{\"q\":\"post data string\"}}' http://localhost:8088/hello/states");
    server::new( move || {
        vec![
            App::with_state( (StateOne { one : 1 }, StateTwo {two:2}) )
                    .prefix("/")
                    .resource("/{param}/states", |r| r.method(http::Method::GET).with2(states))
                    .resource("/{param}/states2", |r| r.method(http::Method::GET).with2(states2))
                    .resource("/{param}/states", |r| r.method(http::Method::POST).with2(post_states))
                    .boxed(),
        ]
    }).bind("127.0.0.1:8088")
        .unwrap()
        .run();
}
